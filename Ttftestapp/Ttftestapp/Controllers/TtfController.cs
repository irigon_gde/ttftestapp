﻿using Contracts;
using DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Ttftestapp.Controllers
{
    public class TtfController : ApiController
    {
        private readonly IEnumerable<ISpecification> _specifications;
        public TtfController(ISpecification[] specifications)
        {
            _specifications = specifications;
        }
        [HttpGet]
        [Route("specification")]
        public IHttpActionResult GetResult([FromUri] InputObject model)
        {
            var saticfiedSpec = _specifications.FirstOrDefault(a => a.isSatisfied(model));
            if (saticfiedSpec == null)
                throw new Exception("Not implemented specification");
            
            return Ok(saticfiedSpec.Calc(model));
        }
    }
}

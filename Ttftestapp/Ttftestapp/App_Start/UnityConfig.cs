﻿using Contracts;
using Microsoft.Practices.Unity;
using SpecificationService;
using System.Linq;
using System.Web.Http;
using Unity.WebApi;

namespace Ttftestapp
{
    public static class UnityConfig
    {
        public static void RegisterComponents(HttpConfiguration http)
        {
			var container = new UnityContainer();

            container.RegisterType<ISpecification, SpecX2R>("SpecX2R", new ContainerControlledLifetimeManager());
            container.RegisterType<ISpecification, SpecX2S>("SpecX2S", new ContainerControlledLifetimeManager());
            container.RegisterType<ISpecification, SpecX2T>("SpecX2T", new ContainerControlledLifetimeManager());
            container.RegisterType<ISpecification, BaseSpacificationX2S>("BaseX2S", new ContainerControlledLifetimeManager());
            container.RegisterType<ISpecification, BaseSpecificationX2R>("BaseX2R", new ContainerControlledLifetimeManager());
            container.RegisterType<ISpecification, BaseSpecificationX2T>("BaseX2T", new ContainerControlledLifetimeManager());
            

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}
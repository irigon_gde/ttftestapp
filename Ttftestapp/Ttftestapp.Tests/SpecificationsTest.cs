﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpecificationService;
using DataContracts;

namespace Ttftestapp.Tests
{
    [TestClass]
    public class SpecificationsTest
    {
        [TestMethod]
        public void TestBaseSpecificationX2S()
        {
            var model = new InputObject(true, true, false, 1, 300, 300);
            var specification = new BaseSpacificationX2S();
            Assert.IsTrue(specification.isSatisfied(model));
            var result = specification.Calc(model);
            Assert.AreEqual(result.X, DataContracts.Enums.XEnum.S);
            Assert.AreEqual(result.Y, 4);
        }
        [TestMethod]
        public void TestBaseSpecificationX2R()
        {
            var model = new InputObject(true, true, true, 1, 300, 100);
            var specification = new BaseSpecificationX2R();
            Assert.IsTrue(specification.isSatisfied(model));
            var result = specification.Calc(model);
            Assert.AreEqual(result.X, DataContracts.Enums.XEnum.R);
            Assert.AreEqual(result.Y, 3);
        }
        [TestMethod]
        public void TestBaseSpecificationX2T()
        {
            var model = new InputObject(false, true, true, 1, 300, 300);
            var specification = new BaseSpecificationX2T();
            Assert.IsTrue(specification.isSatisfied(model));
            var result = specification.Calc(model);
            Assert.AreEqual(result.X, DataContracts.Enums.XEnum.T);
            Assert.AreEqual(result.Y, -2);
        }
        [TestMethod]
        public void TestSpecializedSpecificationX2T()
        {
            var model = new InputObject(true, true, false, 1, 300, 300);
            var specification = new SpecX2T();
            Assert.IsTrue(specification.isSatisfied(model));
            var result = specification.Calc(model);
            Assert.AreEqual(result.X, DataContracts.Enums.XEnum.T);
            Assert.AreEqual(result.Y, -2);
        }
        [TestMethod]
        public void TestSpecializedSpecificationX2R()
        {
            var model = new InputObject(true, true, true, 1, 300, 300);
            var specification = new SpecX2R();
            Assert.IsTrue(specification.isSatisfied(model));
            var result = specification.Calc(model);
            Assert.AreEqual(result.X, DataContracts.Enums.XEnum.R);
            Assert.AreEqual(result.Y, 5);
        }
        [TestMethod]
        public void TestSpecializedSpecificationX2S()
        {
            var model = new InputObject(true, false, true, 1, 300, 300);
            var specification = new SpecX2S();
            Assert.IsTrue(specification.isSatisfied(model));
            var result = specification.Calc(model);
            Assert.AreEqual(result.X, DataContracts.Enums.XEnum.S);
            Assert.AreEqual(result.Y, 304);
        }
    }
}

﻿using DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface ISpecification
    {
        bool isSatisfied(InputObject model);
        OutputObject Calc(InputObject model);
    }
}

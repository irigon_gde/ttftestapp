﻿using DataContracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContracts
{
    public class OutputObject
    {
        public OutputObject(XEnum x, decimal y)
        {
            X = x;
            Y = y;
        }
        public XEnum X { get; private set; }
        public decimal Y { get; private set; }
    }
}

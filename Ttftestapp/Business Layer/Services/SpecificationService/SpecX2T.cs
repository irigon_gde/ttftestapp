﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataContracts;

namespace SpecificationService
{
    public class SpecX2T : BaseSpecificationX2T
    {
        public override bool isSatisfied(InputObject model)
        {
            return model.A && model.B && !model.C;
        }
    }
}

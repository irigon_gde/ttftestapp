﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataContracts;
using DataContracts.Enums;

namespace SpecificationService
{
    public class SpecX2R : BaseSpecificationX2R 
    {
        public override OutputObject Calc(InputObject model)
        {
            var y = 2 * model.D + (model.D * model.E / 100);
            return new OutputObject(XEnum.R, y);
        }
    }
}

﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataContracts;
using DataContracts.Enums;

namespace SpecificationService
{
    public class BaseSpecificationX2R : ISpecification
    {
        public virtual OutputObject Calc(InputObject model)
        {
            var y = model.D + (model.D * (model.E - model.F)/100);
            return new OutputObject(XEnum.R, y);
        }

        public virtual bool isSatisfied(InputObject model)
        {
            return model.A && model.B && model.C;
        }
    }
}

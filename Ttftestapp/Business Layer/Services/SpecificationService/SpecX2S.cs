﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataContracts;
using DataContracts.Enums;

namespace SpecificationService
{
    public class SpecX2S : BaseSpacificationX2S
    {
        public override bool isSatisfied(InputObject model)
        {
            return model.A && !model.B && model.C;
        }
        public override OutputObject Calc(InputObject model)
        {
            var y = model.F + model.D + (model.D * model.E / 100);
            return new OutputObject(XEnum.S, y);
        }
    }
}
